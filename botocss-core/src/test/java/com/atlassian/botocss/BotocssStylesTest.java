package com.atlassian.botocss;

import cz.vutbr.web.css.RuleSet;
import cz.vutbr.web.css.StyleSheet;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.botocss.Classpath.getResource;
import static org.junit.Assert.assertEquals;

public class BotocssStylesTest {
    private static final Logger log = LoggerFactory.getLogger(BotocssStylesTest.class);

    @Test
    public void testParse() {
        BotocssStyles styles = BotocssStyles.parse("body { color: red; }");
        StyleSheet ruleBlocks = styles.getStyleSheets().iterator().next();
        assertEquals(1, ruleBlocks.size());
        RuleSet ruleSet = (RuleSet) ruleBlocks.get(0);
        assertEquals("body", ruleSet.getSelectors()[0].toString());
        assertEquals("color: #ff0000;\n", ruleSet.get(0).toString());
    }

    @Test
    public void testParseValueWithVendorPrefix() {
        BotocssStyles styles = BotocssStyles.parse(".shadow { background-image: -moz-linear-gradient(top, #4687ce 0%, #3068a2 100%); }");
        StyleSheet ruleBlocks = styles.getStyleSheets().iterator().next();
        assertEquals(1, ruleBlocks.size());
        RuleSet ruleSet = (RuleSet) ruleBlocks.get(0);
        assertEquals(".shadow", ruleSet.getSelectors()[0].toString());
        assertEquals("background-image: -moz-linear-gradient(top, #4687ce 0.0%, #3068a2 100.0%);\n", ruleSet.get(0).toString());
    }

    @Test
    public void testParseNotificationStyles() {
        long start = System.currentTimeMillis();
        BotocssStyles styles = BotocssStyles.parse(getResource("daily-summary.css"));
        log.info("Parsing notification styles took {} ms", System.currentTimeMillis() - start);

        StyleSheet ruleBlocks = styles.getStyleSheets().iterator().next();
        assertEquals(74, ruleBlocks.size());
    }
}

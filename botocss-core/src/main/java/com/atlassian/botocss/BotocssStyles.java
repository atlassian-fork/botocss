package com.atlassian.botocss;

import cz.vutbr.web.css.CSSException;
import cz.vutbr.web.css.CSSFactory;
import cz.vutbr.web.css.StyleSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A set of pre-parsed CSS stylesheets, which can be injected with {@link Botocss#inject(String, BotocssStyles)}.
 * This can improve performance where there are many HTML documents to be injected
 * with the same stylesheet.
 */
public final class BotocssStyles {

    /**
     * Can be used to communicate that there's no external CSS information available.
     */
    @SuppressWarnings("unchecked")
    public static final BotocssStyles EMPTY = new BotocssStyles(Collections.EMPTY_LIST);

    private static final Logger log = LoggerFactory.getLogger(BotocssStyles.class);

    private final Iterable<StyleSheet> styleSheets;

    /**
     * Constructs a BotocssStyles object from the provided CSS.
     */
    public static BotocssStyles parse(String... css) {
        List<StyleSheet> styleSheets = new ArrayList<>(css.length);
        long start = System.currentTimeMillis();
        for (String stylesheet : css) {
            try {
                styleSheets.add(CSSFactory.parse(stylesheet));
            } catch (IOException | CSSException e) {
                throw new RuntimeException(e); // if this happens, it indicates a bug
            }

        }
        log.info("Parsing {} stylesheets took {} ms", styleSheets.size(), System.currentTimeMillis() - start);
        return new BotocssStyles(styleSheets);
    }

    private BotocssStyles(Iterable<StyleSheet> styleSheets) {
        this.styleSheets = styleSheets;
    }

    /**
     * Returns the underlying stylesheet objects. Not to be used outside Botocss.
     */
    /* default */ Iterable<StyleSheet> getStyleSheets() {
        return styleSheets;
    }
}
